const blankBand = {
    band_id: undefined,
    band_name: undefined
};
var productCreate = new Vue({
    el: '#product-create',
    data: {
        mainImage: undefined,
        altImages: [],
        subImages: [],
        title: undefined,
        description: undefined,
        price: 0.00,
        quantity: 0,
        quantityAvailable: undefined,
        availableBands: [],
        year: undefined,
        bands: [
            {
                band_name: undefined,
                band_id: undefined
            }
        ],
        venues: [
            {
                venue_name: undefined,
                venue_id: undefined
            }
        ],
        availableVenues: []
    },
    methods: {
        addBand: function(){
            this.bands.push({
                band_name: undefined,
                band_id: undefined
            })
        },
        removeBand: function(index){
            this.bands.splice(index, 1);
        },
        addVenue: function(){
            this.venues.push({
                venue_name: undefined,
                venue_id: undefined
            })
        },
        removeVenue: function(index){
            this.venues.splice(index, 1);
        },
        addAltImage: function(){
            this.altImages.push({
                venue_name: undefined,
                venue_id: undefined
            })
        },
        removeAltImage: function(index){
            this.altImages.splice(index, 1);
        }

    },
    mounted: function(){
        this.$http.get('/api/bands').then(response => {
            this.availableBands = response.body;
        })
        this.$http.get('/api/venues').then(response => {
            this.availableVenues = response.body;
        })
    }
})