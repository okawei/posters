@extends('layouts.admin')


@section('content')
    <div class="container" id="product-create">
        <h1>Create Product</h1>
        <form method="post" enctype="multipart/form-data">
            {{csrf_field()}}
            <div class="row">
                <div class="col-md-6">
                    <label>Main Image</label>
                    <label class="custom-file d-block">
                        <input data-toggle="custom-file" data-target="#main-image" type="file" name="main-image" accept="image/png" class="custom-file-input">
                        <span id="main-image" class="custom-file-control custom-file-name" data-content="Upload main image..."></span>
                    </label>
                    <div class="form-group" v-for="(altImage, index) in altImages">
                        <br>
                        <label class="custom-file d-block">
                            <input data-toggle="custom-file" v-bind:data-target="'#alt-image'+index" type="file" name="alt-images[]" accept="image/png" class="custom-file-input">
                            <span v-bind:id="'alt-image'+index" class="custom-file-control custom-file-name" data-content="Upload alt image..."></span>
                            <small id="altImage" class="form-text text-muted">Alt image #@{{ index+1 }}</small>

                        </label>
                        <a class="btn btn-danger" style="float: right;" @click="removeAltImage(index)">Remove image</a>
                        <br><br>
                    </div>
                    <a class="btn btn-default" @click="addAltImage">+ Add Alternative Image</a>
                </div>
                <div class="col-md-6">
                    <h2>Product</h2>
                    <div class="form-group">
                        <label for="productTitle">Title</label>
                        <input name="title" v-model="title"  class="form-control" id="productTitle" aria-describedby="titleHelp" placeholder="Enter Title">
                        <small id="titleHelp" class="form-text text-muted">The title as it appears on the site.</small>
                    </div>
                    <div class="form-group">
                        <label for="productDescription">Description</label>
                        <textarea name="description" v-model="description" type="email" class="form-control" id="productDescription" aria-describedby="descriptionHelp" placeholder="Enter Description"></textarea>
                        <small id="descriptionHelp" class="form-text text-muted">The description as it appears on the site.</small>
                    </div>
                    <div class="form-group">
                        <label for="productPrice">Price</label>
                        <input name="price" v-model="price" type="number" class="form-control" id="productPrice" aria-describedby="priceHelp" placeholder="Enter Price">
                        <small id="priceHelp" class="form-text text-muted">The price of the product.</small>
                    </div>
                    <div class="form-group">
                        <label for="productQuantity">Quantity</label>
                        <input name="quantity" v-model="quantity" type="number" class="form-control" id="productQuantity" aria-describedby="quantityHelp" placeholder="Enter Quantity">
                        <small id="quantityHelp" class="form-text text-muted">The quantity available of the product.</small>
                    </div>
                    <h2>Bands</h2>
                    <div class="form-group" v-for="(band,index) in bands">
                        <label for="productDescription">Band #@{{ index+1 }}</label>
                        <br>
                        <select name="band_ids[]" v-model="bands[index].band_id" class="form-control">
                            <option v-for="availableBand in availableBands" v-bind:value="availableBand.id">@{{ availableBand.name }}</option>
                        </select>
                        <small id="bandHelp" class="form-text text-muted">Select an existing band.</small>
                        or<br>
                        <input name="band_names[]" v-model="bands[index].band_name"  class="form-control" id="newBandName" aria-describedby="bandHelp" placeholder="Enter band name">
                        <small id="bandHelp" class="form-text text-muted">Add a new band.</small>
                        <a v-show="index != 0" class="btn btn-danger" style="float: right;" @click="removeBand(index)">- Remove Band</a>
                    </div>
                    <a class="btn btn-default" @click="addBand">+ Add another band</a>
                    <h2>Venues</h2>
                    <div class="form-group" v-for="(venue,index) in venues">
                        <label for="productDescription">Venue #@{{ index+1 }}</label>
                        <br>
                        <select name="venue_ids[]" v-model="venues[index].venue_id" class="form-control">
                            <option v-for="availableBand in availableBands" v-bind:value="availableBand.id">@{{ availableBand.name }}</option>
                        </select>
                        <small id="venueHelp" class="form-text text-muted">Select an existing venue.</small>
                        or<br>
                        <input name="venue_names[]" v-model="venues[index].venue_name"  class="form-control" id="newBandName" aria-describedby="venueHelp" placeholder="Enter venue name">
                        <small id="venueHelp" class="form-text text-muted">Add a new venue.</small>
                        <a v-show="index != 0" class="btn btn-danger" style="float: right;" @click="removeVenue(index)">- Remove Venue</a>
                    </div>
                    <a class="btn btn-default" @click="addVenue">+ Add another venue</a>
                    <button class="btn btn-lg btn-primary">Save</button>
                </div>

            </div>

        </form>
    </div>

@stop

