<?php

namespace Posters;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Product extends Model
{
    use SoftDeletes;

    public function collection(){
        return $this->belongsToMany(Collection::class);
    }

    public function bands(){
        return $this->belongsToMany(Band::class);
    }

    public function venues(){
        return $this->belongsToMany(Venue::class);
    }
}
