<?php

namespace Posters;

use Illuminate\Database\Eloquent\Model;

class Band extends Model
{
    public function products(){
        return $this->belongsToMany(Product::class);
    }

    public function venues(){
        return $this->hasManyThrough(Venue::class, Product::class);
    }
}
