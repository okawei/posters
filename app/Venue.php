<?php

namespace Posters;

use Illuminate\Database\Eloquent\Model;

class Venue extends Model
{
    public function products(){
        return $this->belongsToMany(Product::class);
    }
}
