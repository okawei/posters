<?php

namespace Posters\Console\Commands;

use Faker\Factory;
use Illuminate\Console\Command;
use Posters\Band;
use Posters\Collection;
use Posters\Product;
use Posters\ProductImage;
use Posters\User;
use Posters\Venue;

class GenerateTestData extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'data:generate';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Generates test data';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        $faker = Factory::create();
//        Venue::truncate();
//        Band::truncate();
        factory(Venue::class, $faker->numberBetween(10, 50))->create();
        factory(Band::class, $faker->numberBetween(10, 100))->create();
        factory(Product::class, $faker->numberBetween(50, 300))
            ->create()
            ->each(function($product) use ($faker){
                $productImage = ProductImage::inRandomOrder()->first();
                if($productImage == null){
                    $productImage = \factory(ProductImage::class)->create();
                }
                $product->product_image_id = $productImage->id;
                $product->venue_id = Venue::inRandomOrder()->first()->id;
                $product->save();
                $bands = Band::select('id')->inRandomOrder()->take($faker->numberBetween(1, 3))->get()->map(function($band){
                    return $band->id;
                });
                $product->bands()->attach($bands);
            });

        \factory(Collection::class, 10)->create()->each(function($collection) use ($faker){
            $products = array_column(Product::select('id')
                ->inRandomOrder()
                ->take($faker
                    ->numberBetween(3, 10))
                ->get()
                ->toArray(),
            'id'
            );
            $collection->products()->attach($products);
        });
        \factory(User::class)->create([
            'email'=>'heckel.max@gmail.com'
        ]);
    }
}
