<?php

namespace Posters;

use Illuminate\Database\Eloquent\Model;

class Token extends Model
{
    public function carts(){
        return $this->hasMany(Cart::class);
    }

    public function activeCart(){
        return $this->hasOne(Cart::class)->where('status_id', Status::ACTIVE);
    }
}
