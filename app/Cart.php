<?php

namespace Posters;

use Illuminate\Database\Eloquent\Model;

class Cart extends Model
{
    public function token(){
        return $this->belongsTo(Token::class);
    }

    public function items(){
        return $this->hasMany(Item::class);
    }
}
