<?php

namespace Posters\Repositories;

use Posters\Cart;
use Posters\Item;
use Posters\Product;

class CartRepository{
    public function addItem(Cart $cart, Product $product, $quantity){
        $item = $cart->items->where('product_id', $product->id)->first() ?: new Item();
        $item->product_id = $product->id;
        $item->cart_id = $cart->id;
        $item->quantity = $item->quantity ? $item->quantity+$quantity : $quantity;
        $item->save();
    }

    public function validateCart(Cart $cart){
        foreach ($cart->items as $item){
            if($item->product == null || $item->product->quantity == 0){
                $item->delete();
            }
        }
    }
}