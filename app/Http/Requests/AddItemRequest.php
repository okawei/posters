<?php

namespace Posters\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Validation\Rule;
use Illuminate\Validation\ValidationException;
use Posters\Product;

class AddItemRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        $product = Product::find($this->request->get('product_id'));
        if($product == null){
            $validator = \Validator::make(
                [
                    'product'=>null
                ],
                [
                    'product'=>'required'
                ],
                [
                    'required'
                ]
            );
            $this->failedValidation($validator);
        }
        return [
            'product_id' => [
                'required',
                'int'
            ],
            'quantity'=>'int'
        ];
    }
}
