<?php

namespace Posters\Http\Controllers;

use Illuminate\Http\Request;

class HomeController extends Controller
{
    public function getHome(){
        $products = \Posters\Product::orderBy('admin_sort', 'asc')
            ->take(30)
            ->get();
        $collections = \Posters\Collection::orderBy('admin_sort', 'asc')
            ->take(1)
            ->with(['products'=>function($builder){
                $builder->take(30);
            }])->get();
        return view('browse.home', compact('products', 'collections'));
    }
}
