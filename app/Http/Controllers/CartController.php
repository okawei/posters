<?php

namespace Posters\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Session;
use Posters\Cart;
use Posters\Http\Requests\AddItemRequest;
use Posters\Product;
use Posters\Repositories\CartRepository;
use Posters\Token;

class CartController extends Controller
{
    /**
     * @var CartRepository
     */
    private $cartRepository;

    public function __construct(CartRepository $cartRepository)
    {
        $this->cartRepository = $cartRepository;
    }

    public function postAddToCart(AddItemRequest $request, $hash = null){

        $token = ($hash == null)
            ? factory(Token::class)->create()
            : Token::where('hash', $hash)->first();
        $cart = $token->activeCart ?: factory(Cart::class)->create([
            'token_id'=>$token->id
        ]);
        $this->cartRepository->validateCart($cart);
        $product = Product::find($request->product_id);
        $this->cartRepository->addItem($cart, $product, $request->quantity);
        $cart = Cart::with('items', 'token')->where('id', $cart->id)->first();
        return $cart;
    }
}
