<?php

namespace Posters\Http\Controllers;

use Illuminate\Http\Request;
use Posters\Venue;

class VenuesController extends Controller
{
    public function getVenues(){
        return Venue::orderBy('name', 'asc')->get();
    }
}
