<?php

namespace Posters\Http\Controllers\Admin\API;

use Illuminate\Http\Request;
use Posters\Band;
use Posters\Http\Controllers\Controller;

class BandsController extends Controller
{
    public function getBands(){
        return Band::orderBy('name', 'asc')->get();
    }

    public function createBand(Request $request){

    }
}
