<?php

namespace Posters\Http\Controllers\Admin;

use Illuminate\Http\Request;
use Posters\Http\Controllers\Controller;

class ProductController extends Controller
{
    public function getCreateProducts(){
        return view('admin.products.create');
    }
    public function postCreateProducts(Request $request){
        dd($request->all());
    }
}
