<?php

namespace Posters\Http\Controllers\Admin;

use Illuminate\Http\Request;
use Posters\Http\Controllers\Controller;

class DashboardController extends Controller
{
    public function getDashboard(){
        return view('admin.dashboard');
    }
}
