<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', 'HomeController@getHome');


Route::group(['prefix'=>'admin'], function(){
    Route::get('', 'Auth\LoginController@showLoginForm')->name('login');
    Route::post('', 'Auth\LoginController@login');
    Route::get('/forgot', 'Auth\ForgotPasswordController@showLinkRequestForm')->name('password.request');
    Route::post('/forgot', 'Auth\ForgotPasswordController@sendResetLinkEmail')->name('password.email');
    Route::get('/password/reset/{token}', 'Auth\ResetPasswordController@showResetForm')->name('password.reset');
    Route::post('/password/reset', 'Auth\ResetPasswordController@reset');


});

Route::group(['middleware'=>'auth', 'prefix'=>'admin/dashboard'], function(){
    Route::get('/', 'Admin\DashboardController@getDashboard');
    Route::group(['prefix'=>'products'], function(){
        Route::get('/create', 'Admin\ProductController@getCreateProducts')->name('admin.products.create');
        Route::post('/create', 'Admin\ProductController@postCreateProducts');
    });
});

Route::post('/logout', 'Auth\LoginController@logout')->name('logout');