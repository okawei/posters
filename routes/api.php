<?php

use Illuminate\Http\Request;
use Posters\Http\Controllers\Admin\API\BandsController;
use Posters\Http\Controllers\VenuesController;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::middleware('auth:api')->get('/user', function (Request $request) {
    return $request->user();
});
Route::post('/cart/{token?}', 'CartController@postAddToCart');
Route::group(['middleware'=>'auth'], function(){
    Route::get('/bands', "\\". BandsController::class.'@getBands');
    Route::get('/venues', "\\". VenuesController::class.'@getVenues');
});

