<?php

namespace Tests\Feature;

use Faker\Factory;
use Posters\Cart;
use Posters\Item;
use Posters\Product;
use Posters\Token;
use Tests\TestCase;
use Illuminate\Foundation\Testing\RefreshDatabase;

class CartControllerTest extends TestCase
{

    /** @var \Faker\Generator  */
    private $faker;

    public function __construct($name = null, array $data = [], $dataName = '')
    {
        parent::__construct($name, $data, $dataName);
        $this->faker = Factory::create();
    }

    public function test_it_adds_to_cart_without_token(){
        Token::truncate();
        Cart::truncate();
        Item::truncate();
        /** @var Product $product */
        list($product, $quantity, $addItemBody) = $this->generateAddItemBody();
        $this->post('/api/cart', $addItemBody)->assertSuccessful();

        $token = Token::first();
        $this->assertNotNull($token);
        $this->assertNotNull($token->activeCart);
        $this->assertEquals(
            $quantity, $token
                ->activeCart
                ->items
                ->where('product_id', $product->id)
                ->first()
                ->quantity
        );
    }

    public function test_it_adds_to_cart_with_token_no_cart(){
        /** @var Token $token */
        $token = \factory(Token::class)->create();
        list($product, $quantity, $addItemBody) = $this->generateAddItemBody();
        $this->post('/api/cart/'.$token->hash, $addItemBody)->assertSuccessful();
        $this->assertEquals(
            $quantity, $token
            ->activeCart
            ->items
            ->where('product_id', $product->id)
            ->first()
            ->quantity
        );
    }

    public function test_it_adds_to_cart_with_token_with_cart(){
        /** @var Token $token */
        $token = \factory(Token::class)->create();
        \factory(Cart::class)->create([
            'token_id'=>$token->id
        ]);
        list($product, $quantity, $addItemBody) = $this->generateAddItemBody();
        $this->post('/api/cart/'.$token->hash, $addItemBody)->assertSuccessful();
        $this->assertEquals(
            $quantity, $token
            ->activeCart
            ->items
            ->where('product_id', $product->id)
            ->first()
            ->quantity
        );
    }

    public function test_it_adds_to_quantity_for_existing_items(){
        /** @var Token $token */
        list($token, $product, $quantity, $addItemBody, $startingQuantity) = $this->generateTokenWithCartAndItems();
        $this->post('/api/cart/'.$token->hash, $addItemBody)->assertSuccessful();
        $this->assertEquals(
            $quantity+$startingQuantity, $token
            ->activeCart
            ->items
            ->where('product_id', $product->id)
            ->first()
            ->quantity
        );
    }

    public function test_it_removes_items_from_cart_with_no_quantity(){
        list($token, $product, $quantity, $addItemBody, $startingQuantity) = $this->generateTokenWithCartAndItems();
        $product->quantity = 0;
        $product->save();
        $this->post('/api/cart/'.$token->hash, $addItemBody)->assertSuccessful();
        $this->assertEquals(0, $token->activeCart->items->count());
    }




    /**
     * @return array
     */
    private function generateAddItemBody()
    {
        $product = Product::inRandomOrder()->first();
        $quantity = $product->quantity - $this->faker->numberBetween(0, $product->quantity - 1);
        $addItemBody = [
            'product_id' => $product->id,
            'quantity' => $quantity
        ];
        return array($product, $quantity, $addItemBody);
    }

    /**
     * @return array
     */
    private function generateTokenWithCartAndItems()
    {
        $token = \factory(Token::class)->create();
        \factory(Cart::class)->create([
            'token_id' => $token->id
        ]);
        list($product, $quantity, $addItemBody) = $this->generateAddItemBody();
        $startingQuantity = $this->faker->numberBetween(1, 5);
        \factory(Item::class)->create([
            'product_id' => $product->id,
            'quantity' => $startingQuantity,
            'cart_id' => $token->activeCart->id
        ]);
        return array($token, $product, $quantity, $addItemBody, $startingQuantity);
    }
}
