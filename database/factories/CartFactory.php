<?php

use Faker\Generator as Faker;
use Posters\Cart;

/* @var Illuminate\Database\Eloquent\Factory $factory */

$factory->define(Cart::class, function (Faker $faker) {
    return [
        'token_id'=> \Posters\Token::inRandomOrder()->first()
            ? \Posters\Token::inRandomOrder()->first()->id
            : factory(\Posters\Token::class)->create()->id
    ];
});
