<?php

use Faker\Generator as Faker;
use Posters\Token;

/* @var Illuminate\Database\Eloquent\Factory $factory */

$factory->define(Token::class, function (Faker $faker) {
    $hash = null;
    while($hash == null){
        $hash = str_random(64);
        $check = Token::where('hash', $hash)->first();
        if($check != null){
            $hash = null;
        }
    }
    return [
        'hash'=>$hash
    ];
});
