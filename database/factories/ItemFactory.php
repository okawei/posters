<?php

use Faker\Generator as Faker;

/* @var Illuminate\Database\Eloquent\Factory $factory */

$factory->define(\Posters\Item::class, function (Faker $faker) {
    return [
        'quantity' => $faker->numberBetween(1, 10),
        'product_id' => \Posters\Product::inRandomOrder()->first() ? \Posters\Product::inRandomOrder()->first()->id : factory(\Posters\Product::class)->create()->id,
        'cart_id' => \Posters\Cart::inRandomOrder()->first() ? \Posters\Cart::inRandomOrder()->first()->id : factory(\Posters\Cart::class)->create()->id
    ];
});
