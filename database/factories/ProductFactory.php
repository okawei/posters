<?php

use Faker\Generator as Faker;

/* @var Illuminate\Database\Eloquent\Factory $factory */

$factory->define(\Posters\Product::class, function (Faker $faker) {
    return [
        'title'=>$faker->sentence,
        'description'=>$faker->paragraph,
        'price' => $faker->randomFloat(2, 10, 3000),
        'quantity' => $faker->numberBetween(0, 100),
        'admin_sort' => $faker->numberBetween(0, 100),
        'product_image_id' => 0,
        'year' => $faker->numberBetween(1930, 2017). $faker->boolean ? " - " .$faker->numberBetween(2010, 2017) : '',
        'venue_id' => 0,
        'deleted_at' => null
    ];
});

