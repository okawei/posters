<?php

use Faker\Generator as Faker;

/* @var Illuminate\Database\Eloquent\Factory $factory */

$factory->define(\Posters\ProductImage::class, function (Faker $faker) {
    $product = \Posters\Product::inRandomOrder()->first();
    if($product == null){
        $product = factory(\Posters\Product::class)->create();
    }
    return [
        'image_path'=>$faker->imageUrl(),
        'product_id' => $product
    ];
});
