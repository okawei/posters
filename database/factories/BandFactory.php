<?php

use Faker\Generator as Faker;

/* @var Illuminate\Database\Eloquent\Factory $factory */

$factory->define(\Posters\Band::class, function (Faker $faker) {
//    $name = json_decode(file_get_contents('http://bandname.filiplundby.dk/api/v1/sentence'));
//    $name = $name[0];
    return [
        'name' => $faker->word
    ];
});
