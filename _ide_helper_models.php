<?php
/**
 * A helper file for your Eloquent Models
 * Copy the phpDocs from this file to the correct Model,
 * And remove them from this file, to prevent double declarations.
 *
 * @author Barry vd. Heuvel <barryvdh@gmail.com>
 */


namespace Posters{
/**
 * Posters\Address
 *
 * @property int $id
 * @property \Carbon\Carbon|null $created_at
 * @property \Carbon\Carbon|null $updated_at
 * @property string $street1
 * @property string|null $street2
 * @property string $city
 * @property string $state
 * @property string $zip
 * @property string $country
 * @method static \Illuminate\Database\Eloquent\Builder|\Posters\Address whereCity($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\Posters\Address whereCountry($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\Posters\Address whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\Posters\Address whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\Posters\Address whereState($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\Posters\Address whereStreet1($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\Posters\Address whereStreet2($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\Posters\Address whereUpdatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\Posters\Address whereZip($value)
 */
	class Address extends \Eloquent {}
}

namespace Posters{
/**
 * Posters\Band
 *
 * @property int $id
 * @property \Carbon\Carbon|null $created_at
 * @property \Carbon\Carbon|null $updated_at
 * @property string $name
 * @property-read \Illuminate\Database\Eloquent\Collection|\Posters\Product[] $products
 * @property-read \Illuminate\Database\Eloquent\Collection|\Posters\Venue[] $venues
 * @method static \Illuminate\Database\Eloquent\Builder|\Posters\Band whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\Posters\Band whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\Posters\Band whereName($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\Posters\Band whereUpdatedAt($value)
 */
	class Band extends \Eloquent {}
}

namespace Posters{
/**
 * Posters\Cart
 *
 * @property int $id
 * @property \Carbon\Carbon|null $created_at
 * @property \Carbon\Carbon|null $updated_at
 * @property int $token_id
 * @property int $transaction_id
 * @property-read \Illuminate\Database\Eloquent\Collection|\Posters\Item[] $items
 * @property-read \Posters\Token $token
 * @method static \Illuminate\Database\Eloquent\Builder|\Posters\Cart whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\Posters\Cart whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\Posters\Cart whereTokenId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\Posters\Cart whereTransactionId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\Posters\Cart whereUpdatedAt($value)
 */
	class Cart extends \Eloquent {}
}

namespace Posters{
/**
 * Posters\Collection
 *
 * @property int $id
 * @property \Carbon\Carbon|null $created_at
 * @property \Carbon\Carbon|null $updated_at
 * @property string|null $deleted_at
 * @property string $title
 * @property string $description
 * @property int $admin_sort
 * @property-read \Illuminate\Database\Eloquent\Collection|\Posters\Product[] $products
 * @method static \Illuminate\Database\Eloquent\Builder|\Posters\Collection whereAdminSort($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\Posters\Collection whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\Posters\Collection whereDeletedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\Posters\Collection whereDescription($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\Posters\Collection whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\Posters\Collection whereTitle($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\Posters\Collection whereUpdatedAt($value)
 */
	class Collection extends \Eloquent {}
}

namespace Posters{
/**
 * Posters\Coupon
 *
 * @property int $id
 * @property \Carbon\Carbon|null $created_at
 * @property \Carbon\Carbon|null $updated_at
 * @property string $promo_type
 * @property float $minimum_cart_value
 * @property string $code
 * @method static \Illuminate\Database\Eloquent\Builder|\Posters\Coupon whereCode($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\Posters\Coupon whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\Posters\Coupon whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\Posters\Coupon whereMinimumCartValue($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\Posters\Coupon wherePromoType($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\Posters\Coupon whereUpdatedAt($value)
 */
	class Coupon extends \Eloquent {}
}

namespace Posters{
/**
 * Posters\Item
 *
 * @property int $id
 * @property \Carbon\Carbon|null $created_at
 * @property \Carbon\Carbon|null $updated_at
 * @property string|null $deleted_at
 * @property int $product_id
 * @property int $cart_id
 * @property float $purchase_price
 * @method static \Illuminate\Database\Eloquent\Builder|\Posters\Item whereCartId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\Posters\Item whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\Posters\Item whereDeletedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\Posters\Item whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\Posters\Item whereProductId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\Posters\Item wherePurchasePrice($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\Posters\Item whereUpdatedAt($value)
 */
	class Item extends \Eloquent {}
}

namespace Posters{
/**
 * Posters\Product
 *
 * @property int $id
 * @property \Carbon\Carbon|null $created_at
 * @property \Carbon\Carbon|null $updated_at
 * @property string|null $deleted_at
 * @property string $title
 * @property float $price
 * @property int $quantity
 * @property int $admin_sort
 * @property string $description
 * @property int $product_image_id
 * @property string $year
 * @property int $venue_id
 * @property-read \Illuminate\Database\Eloquent\Collection|\Posters\Band[] $bands
 * @property-read \Illuminate\Database\Eloquent\Collection|\Posters\Collection[] $collection
 * @property-read \Posters\Venue $venue
 * @method static \Illuminate\Database\Eloquent\Builder|\Posters\Product whereAdminSort($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\Posters\Product whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\Posters\Product whereDeletedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\Posters\Product whereDescription($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\Posters\Product whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\Posters\Product wherePrice($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\Posters\Product whereProductImageId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\Posters\Product whereQuantity($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\Posters\Product whereTitle($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\Posters\Product whereUpdatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\Posters\Product whereVenueId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\Posters\Product whereYear($value)
 */
	class Product extends \Eloquent {}
}

namespace Posters{
/**
 * Posters\ProductImage
 *
 * @property int $id
 * @property \Carbon\Carbon|null $created_at
 * @property \Carbon\Carbon|null $updated_at
 * @property string|null $deleted_at
 * @property string $image_path
 * @property int $product_id
 * @property int $sort
 * @method static \Illuminate\Database\Eloquent\Builder|\Posters\ProductImage whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\Posters\ProductImage whereDeletedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\Posters\ProductImage whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\Posters\ProductImage whereImagePath($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\Posters\ProductImage whereProductId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\Posters\ProductImage whereSort($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\Posters\ProductImage whereUpdatedAt($value)
 */
	class ProductImage extends \Eloquent {}
}

namespace Posters{
/**
 * Posters\Shipment
 *
 * @property int $id
 * @property \Carbon\Carbon|null $created_at
 * @property \Carbon\Carbon|null $updated_at
 * @property int $shipping_address_id
 * @property int $billing_address_id
 * @property int $transaction_id
 * @property int $status_id
 * @method static \Illuminate\Database\Eloquent\Builder|\Posters\Shipment whereBillingAddressId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\Posters\Shipment whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\Posters\Shipment whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\Posters\Shipment whereShippingAddressId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\Posters\Shipment whereStatusId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\Posters\Shipment whereTransactionId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\Posters\Shipment whereUpdatedAt($value)
 */
	class Shipment extends \Eloquent {}
}

namespace Posters{
/**
 * Posters\Status
 *
 * @property int $id
 * @property \Carbon\Carbon|null $created_at
 * @property \Carbon\Carbon|null $updated_at
 * @property string $short_name
 * @property string $name
 * @method static \Illuminate\Database\Eloquent\Builder|\Posters\Status whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\Posters\Status whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\Posters\Status whereName($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\Posters\Status whereShortName($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\Posters\Status whereUpdatedAt($value)
 */
	class Status extends \Eloquent {}
}

namespace Posters{
/**
 * Posters\Token
 *
 * @property int $id
 * @property \Carbon\Carbon|null $created_at
 * @property \Carbon\Carbon|null $updated_at
 * @property string $hash
 * @property-read \Illuminate\Database\Eloquent\Collection|\Posters\Cart[] $carts
 * @method static \Illuminate\Database\Eloquent\Builder|\Posters\Token whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\Posters\Token whereHash($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\Posters\Token whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\Posters\Token whereUpdatedAt($value)
 */
	class Token extends \Eloquent {}
}

namespace Posters{
/**
 * Posters\Transaction
 *
 * @property int $id
 * @property \Carbon\Carbon|null $created_at
 * @property \Carbon\Carbon|null $updated_at
 * @property int $cart_id
 * @property string $purchase_date
 * @property float $purchase_price
 * @property int $status_id
 * @method static \Illuminate\Database\Eloquent\Builder|\Posters\Transaction whereCartId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\Posters\Transaction whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\Posters\Transaction whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\Posters\Transaction wherePurchaseDate($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\Posters\Transaction wherePurchasePrice($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\Posters\Transaction whereStatusId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\Posters\Transaction whereUpdatedAt($value)
 */
	class Transaction extends \Eloquent {}
}

namespace Posters{
/**
 * Posters\User
 *
 * @property int $id
 * @property string $name
 * @property string $email
 * @property string $password
 * @property string|null $remember_token
 * @property \Carbon\Carbon|null $created_at
 * @property \Carbon\Carbon|null $updated_at
 * @property-read \Illuminate\Notifications\DatabaseNotificationCollection|\Illuminate\Notifications\DatabaseNotification[] $notifications
 * @method static \Illuminate\Database\Eloquent\Builder|\Posters\User whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\Posters\User whereEmail($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\Posters\User whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\Posters\User whereName($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\Posters\User wherePassword($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\Posters\User whereRememberToken($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\Posters\User whereUpdatedAt($value)
 */
	class User extends \Eloquent {}
}

namespace Posters{
/**
 * Posters\Venue
 *
 * @property int $id
 * @property \Carbon\Carbon|null $created_at
 * @property \Carbon\Carbon|null $updated_at
 * @property string $name
 * @property-read \Illuminate\Database\Eloquent\Collection|\Posters\Product[] $products
 * @method static \Illuminate\Database\Eloquent\Builder|\Posters\Venue whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\Posters\Venue whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\Posters\Venue whereName($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\Posters\Venue whereUpdatedAt($value)
 */
	class Venue extends \Eloquent {}
}

